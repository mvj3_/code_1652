//
//  Thingie.h
//  Learning0510 plist
//
//  Created by  apple on 13-5-10.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Thingie : NSObject  <NSCoding>
{
    NSString * name;
    int magicNumber;
    float shoeSize;
    NSMutableArray *subThingies;
}

@property (copy) NSString * name;
@property int magicNumber;
@property float shoeSize;
@property (retain) NSMutableArray *subThingies;

-(id)initWithName:(NSString *) n magicNumber:(int) mn shoeSize: (float) ss;

@end
