//
//  Thingie.m
//  Learning0510 plist
//
//  Created by  apple on 13-5-10.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import "Thingie.h"

@implementation Thingie
@synthesize name;
@synthesize magicNumber;
@synthesize shoeSize;
@synthesize subThingies;

-(id)initWithName:(NSString *) n magicNumber:(int) mn shoeSize: (float) ss{
    if(self = [super init]){
        self.name = n;
        self.magicNumber = mn;
        self.shoeSize = ss;
        self.subThingies = [NSMutableArray array];
    }
    return self;
}

-(NSString *)description{
    NSString * desc;
    desc = [NSString stringWithFormat:@"%@, %d/%.1f %@", name, magicNumber, shoeSize, subThingies ];
    return desc;
}

- (void)dealloc
{
    [name release];
    [subThingies release];
    [super dealloc];
}

- (id)initWithCoder:(NSCoder *)coder
{
   // self = [super initWithCoder:coder];
    self = [super init]; //因为NSObject没有实现NSCoding协议，所以不能使用super initWithCoder:
    if (self) {
        self.name = [coder decodeObjectForKey:@"name"];
        self.magicNumber = [coder decodeIntForKey:@"magicNumber"];
        self.shoeSize = [coder decodeFloatForKey:@"shoeSize"];
        self.subThingies = [coder decodeObjectForKey:@"subThingies"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)coder{
    [coder encodeObject:name forKey:@"name"];
    [coder encodeInt:magicNumber forKey:@"magicNumber"];
    [coder encodeFloat:shoeSize forKey:@"shoeSize"];
    [coder encodeObject:subThingies forKey:@"subThingies"];
}
@end
