//
//  main.m
//  Learning0510 plist
//
//  Created by  apple on 13-5-10.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Thingie.h"
int main(int argc, const char * argv[])
{

    @autoreleasepool {
        //====================    plist    =======================
        NSString * filename = @"/tmp/verbiage.txt";
        NSArray * phrase;
        phrase = [NSArray arrayWithObjects:@"I", @"seem", @"to", @"be", @"a", @"verb", nil];
        NSLog(@"%@", phrase);
        
        //将NSArray的内容保存到文件
        [phrase writeToFile:filename atomically:true];
        
        //从文件中读取内容，保存到NSArray
        NSArray *contents = [NSArray arrayWithContentsOfFile:filename];
        NSLog(@"%@", contents);
        
        //================    encode/decode    ====================
        //初始化对象
        Thingie *thing1;
        thing1 = [[Thingie alloc] initWithName:@"thing1" magicNumber:42 shoeSize:10.5];
        
        //thing1中添加了自身，循环
        [thing1.subThingies addObject:thing1];
        
       // NSLog(@"%@", thing1); //NSLog不能检查自身循环，会执行无限递归
        
        //自身循环时，调用编码、解码依然没有问题，但是如何查看文件呢？文件打不开。
        //如果也不能写入到文件中，那么自身循环有什么意义呢？就算可以编码，不能使用啊。
        
        //编码对象，写入文件
        NSData *freezeDried;
        freezeDried = [NSKeyedArchiver archivedDataWithRootObject:thing1];
        [freezeDried writeToFile:filename atomically:true];
        [thing1 release];
        
        //从文件读取内容，解码对象
        NSData * data = [NSData dataWithContentsOfFile:filename];
        thing1 = [NSKeyedUnarchiver unarchiveObjectWithData:data];
       // NSLog(@"%@", thing1);  
    }
    return 0;
}

